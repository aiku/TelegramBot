#!/usr/bin/python
# coding=utf-8


###   TODO
    #       Invio messaggio a gruppo da chat privata
    #       Dadi esadecimali e ottali 
    #       ???
    #       ???
    #       ???
    #       ???

# Creazione bot
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, ConversationHandler
from telegram import Chat, InlineKeyboardMarkup, InlineKeyboardButton
with open('fileComandi/token.txt', 'r+') as fileToken:
    token = fileToken.read().replace('\n','')

# Librerie base
import os, math, time, random
from os import listdir
from os.path import isfile, join
from datetime import datetime
from time import strftime
import traceback

# start, help, info ed interject sono funzioni che leggono i file corrispondenti e ne stampano il contenuto.
def start(bot, update):
    with open("fileComandi/start.txt", "r") as fileStart:
        update.message.reply_text(fileStart.read(), parse_mode="markdown")
    fileStart.close()
    log(bot, update, "ha avviato il bot.")
    return

def help(bot, update):
    if not update.message.chat.type == Chat.PRIVATE:
        with open("fileComandi/help_group.txt", "r") as fileHelpGroup:
            update.message.reply_text(fileHelpGroup.read(), parse_mode="markdown")
        fileHelpGroup.close()
        log(bot, update, "ha chiesto aiuto.")
        return
    else:
        with open("fileComandi/help.txt", "r") as fileHelpPvt:
            update.message.reply_text(fileHelpPvt.read(), parse_mode="markdown")
        fileHelpPvt.close()
        log(bot, update, "ha chiesto aiuto.")
        return

def info(bot, update):
    if not update.message.chat.type == Chat.PRIVATE:
        update.message.reply_text("Questo comando è disponibile solo nelle chat private!")
        return
    else:
        with open("fileComandi/info.txt", "r") as fileInfo:
            update.message.reply_text(fileInfo.read(), parse_mode="markdown")
        fileInfo.close()
        log(bot, update, "ha chiesto informazioni.")
    return

# Funzione che diffonde il verbo
def interject(bot, update):
    rand_file(bot, update, "fileComandi/interject")
    log(bot, update, "è stato istruito sul verbo.")
    return

# Funzione che cambia lo stato della ripetizione del testo
def echo(bot, update):
    if not update.message.chat.type == Chat.PRIVATE:
        update.message.reply_text("Questo comando è disponibile solo nelle chat private!")
        return
    else:
        with open("log/utentiEcho.txt", "r") as fileEcho:
            utentiEcho = fileEcho.read().splitlines()

        # Se l'utente è già nella lista degli utentiEcho viene rimosso 
        if update.message.chat.username in utentiEcho:
            utentiEcho.remove(update.message.chat.username)
            with open("log/utentiEcho.txt", "w+") as fileEcho:
                fileEcho.writelines(utentiEcho)
            update.message.reply_text("Ripetizione dei messaggi disattivata.")
            log(bot, update, "ha cambiato lo stato di echo.")
        # Altrimenti viene aggiunto alla lista
        else:
            utentiEcho.append(update.message.chat.username)
            with open("log/utentiEcho.txt", "w+") as fileEcho:
                fileEcho.writelines(utentiEcho)
            update.message.reply_text("Ripetizione dei messaggi attivata.")
            log(bot, update, "ha cambiato lo stato di echo.")
        fileEcho.close()
        return


# Funzione che invia un'immagine specifica
def img(bot, update):
    if not update.message.chat.type == Chat.PRIVATE:
        update.message.reply_text("Questo comando è disponibile solo nelle chat private!")
        return
    else:
        listaImmagini = [f for f in listdir("Immagini") if isfile(join("Immagini", f))]
        # Divide il messaggio dopo lo spazio, per ottenere solo la parte con il parametro
        file = update.message.text.split(' ')[1]
        # Se il parametro è nella listaImmagini allora manda l'immagine
        if any(file in s for s in listaImmagini):
            bot.send_document(chat_id=update.message.chat_id, document=open("Immagini/{}".format(file), "rb"))
            log(bot, update, "ha richiesto l'immagine \"{}\"".format(file))
        # Altrimenti, notifica che l'immagine non esiste
        else:
            update.message.reply_text("Quel file non esiste.")
            log(bot, update, "ha richiesto un'immagine inesistente : {}".format(file))
        return

# Funzione che invia un'immagine, comprimendola
def img_pic(bot, update):
    if not update.message.chat.type == Chat.PRIVATE:
        update.message.reply_text("Questo comando è disponibile solo nelle chat private!")
        return

    listaImmagini = [f for f in listdir("Immagini") if isfile(join("Immagini", f))]
    # Se ci sono immagini disponibili, ne sceglie una randomicamente
    if len(listaImmagini) > 0:
        nomeFile = listaImmagini[random.randint(0, len(listaImmagini) - 1)]
        bot.send_photo(chat_id=update.message.chat_id, photo=open("Immagini/{}".format(nomeFile), "rb"))
        log(bot, update, "ha richiesto un'immagine.");
        return
    # Altrimenti, notifica che non ci sono immagini
    else:
        update.message.reply_text("Al momento non ci sono immagini in archivio.")
        log(bot, update, "non ha potuto ricevere un'immagine.")
        return

# Funzione che invia un'immagine senza compressione
def img_file(bot, update):
    if not update.message.chat.type == Chat.PRIVATE:
        update.message.reply_text("Questo comando è disponibile solo nelle chat private!")
        return
    else:
        # Se ci sono immagini disponibili, ne sceglie una randomicamente
        listaImmagini = [f for f in listdir("Immagini") if isfile(join("Immagini", f))]
        if len(listaImmagini) > 0:
            nomeFile = listaImmagini[random.randint(0, len(listaImmagini) - 1)]
            bot.send_document(chat_id=update.message.chat_id, document=open("Immagini/{}".format(nomeFile), "rb"))
            log(bot, update, "ha richiesto un'immagine non compressa.");
            return
        # Altrimenti, notifica che non ci sono immagini
        else:
            update.message.reply_text("Al momento non ci sono immagini in archivio.")
            log(bot, update, "non ha potuto ricevere un'immagine.")
            return

# Funzione che fa uscire il bot dal gruppo se chi lo richiede è nella lista mod
def quit(bot, update):
    if update.message.chat.type == Chat.PRIVATE:
        update.message.reply_text("Come posso uscire da una chat privata ... ?")
        log(bot, update, "ha usato /esci in privata. LOL")
        return
    else:
        with open('fileComandi/mod.txt', "rb") as fileMod:
            listaMod = fileMod.read().splitlines()
            # Se l'utente è nella lista mod il bot abbandona il gruppo
            if update.message.from_user.username in listaMod:
                update.message.reply_text("Addio!")
                log(bot, update, "ha richiesto al bot di uscire da {}".format(update.message.chat_id))
                bot.leave_chat(chat_id=update.message.chat_id)
            # Altrimenti non esce ma segnala l'accaduto
            else:
                update.message.reply_text("Non hai questo potere su di me.")
                log(bot, update, "ha tentato di far uscire il bot da {}".format(update.message.chat_id))
    return

# Funzione che invia il risultato di un tiro di n dadi a n facce
def die(bot, update):
    try:
        throws = int(update.message.text[update.message.text.index(" "):update.message.text.rindex("d")])
        faces = int(update.message.text[update.message.text.rindex("d")+1:])
    except ValueError:
        update.message.reply_text("Non posso fare questo tiro 😕")
        log(bot, update, "ha richiesto un tiro di dadi non valido.")
        return

    if throws > 0 and faces > 0 and throws < 101 and faces < 1000000001:
        results = [  ]
        for i in range(0, throws):
            results.append(random.randint(1, faces))
        results_toprint = ', '.join(str(x) for x in results)
        update.message.reply_text('Ho tirato {} d{} :\n{}'.format(throws, faces, results_toprint))
        log(bot, update, "ha richiesto il tiro di {} d{}.".format(throws, faces))
        return
    else:
        update.message.reply_text("Non posso fare questo tiro 😕")
        log(bot, update, "ha richiesto un tiro di dadi non valido.")
        return

# Funzione "/send [user_id] ; messaggio" che permette di inviare un messaggio ad un utente dal bot conoscendo il suo user_id
def send_to_id(bot, update):
    try:
        with open('fileComandi/mod.txt', "rb") as fileMod:
            listaMod = fileMod.read().splitlines()
            # Se l'utente è nella lista mod il bot invia il messaggio 
            if update.message.from_user.username in listaMod:
                id_recipient = update.message.text[update.message.text.index(" ")+1:update.message.text.index(";")]
                message = str(update.message.text[update.message.text.index(";")+1:])
                bot.send_message(chat_id=id_recipient, text=message)
                log(bot, update, "ha inviato \"{}\" a {}".format(message, id_recipient))
                return
            else:
                update.message.reply_text("Non sei autorizzato ad usare questo comando.")
    except ValueError:
        update.message.reply_text("Non posso inviare questo messaggio 😕")
        return

# Funzione che richiama la tastiera e ne usa il valore per scegliere quale funzione usare per scaricare il file
def download_method(bot, update):
    query = update.callback_query
    bot.edit_message_text(text="Invia pure!", chat_id=query.message.chat_id, message_id=query.message.message_id)
    choices = {
                download_video      (bot, update),
                download_photo      (bot, update),
                download_voice      (bot, update),
                download_document   (bot, update)
              }
    choices(query.data)
    log(bot, update, "ha usato la tastiera per inviare un file.")
    return

# Funzione che invia una tastiera e restituisce il valore relativo al tasto premuto
def keyboard(bot, update):
    if not update.message.chat.type == Chat.PRIVATE:
        update.message.reply_text("Questo comando è disponibile solo nelle chat private!")
        return
    else:
        keyboard = [ [InlineKeyboardButton("Video", callback_data=1)], [InlineKeyboardButton("Messaggio vocale", callback_data=2)],
                    [InlineKeyboardButton("Immagine", callback_data=3)], [InlineKeyboardButton("File", callback_data=4)]]
        keyboard_markup = InlineKeyboardMarkup(keyboard)
        bot.send_message(chat_id=update.message.chat_id, text="Che tipo di file vuoi inviare?", reply_markup=keyboard_markup)
        return

# Funzione che scarica un video se inviato senza alcun comando e viene usato da download_method()
def download_video(bot, update):
    if update.message.chat.type == Chat.PRIVATE:
        file = bot.get_file(update.message.video.file_id)
        mime = update.message.video.mime_type
        ext = mime[mime.rfind("/") + 1:]
        file.download("files/{}.{}".format(str(datetime.now()), ext))
        update.message.reply_text("File salvato!", reply_to_message_id=True)
        log(bot, update, "ha mandato un video.")

# Funzione che scarica una foto se inviata senza alcun comando e viene usato da download_method()
def download_photo(bot, update):
    if update.message.chat.type == Chat.PRIVATE:
        file = bot.get_file(update.message.photo[-1].file_id)
        file.download("files/{}.png".format(str(datetime.now())))
        update.message.reply_text("File salvato!")
        log(bot, update, "ha mandato un'immagine.")
    return

# Funzione che scarica un messaggio vocale se inviato senza comando e viene chiamato da download_method()
def download_voice(bot, update):
    if update.message.chat.type == Chat.PRIVATE:
        file = bot.get_file(update.message.voice.file_id)
        mime = update.message.voice.mime_type
        ext = mime[mime.rfind("/") + 1:]
        file.download("files/{}.{}".format(str(datetime.now()), ext))
        update.message.reply_text("File salvato!")
        log(bot, update, "ha mandato un messaggio vocale.")
    return

# Funzione che scarica file di altra natura se inviati senza comandi e viene chiamata da download_method()
def download_document(bot, update):
    if update.message.chat.type == Chat.PRIVATE:
        file = bot.get_file(update.message.document.file_id)
        mime = update.message.document.mime_type
        ext = mime[mime.rfind("/") + 1:]
        file.download("files/{}.{}".format(str(datetime.now()), ext))
        update.message.reply_text("File salvato!")
        log(bot, update, "ha mandato un file.")
    return

# Funzione che permette di modificare come appare l'username di un utente su telegram
def change_username(bot, update):
    try:
        id=update.message.text[update.message.text.index(";")+1:]
        msg=update.message.text[update.message.text.index(" ")+1:update.message.text.index(";")]
    except:
        update.message.reply_text("Non posso farlo 😕")

    bot.send_message(chat_id=update.message.chat_id, text="[@{}](t.me/{})".format(msg, id), parse_mode="markdown", disable_web_page_preview=True)
    log(bot, update, "ha richiesto la modifica di un username.")
    return





# Estrazione file randomico dalla cartella dir
def rand_file(bot, update, dir):
    listaFile = [f for f in listdir(dir) if isfile(join(dir, f))]
    rand=random.randint(0, len(listaFile)-1)
    with open("{}/{}".format(dir,listaFile[rand]), "r") as fileRand:
        str = fileRand.read()
        try:
            bot.send_message(chat_id=update.message.chat_id, text=str, parse_mode="markdown", reply_to_message_id=update.message.reply_to_message.message_id)
        except:
            bot.send_message(chat_id=update.message.chat_id, text=str, parse_mode="markdown")
        fileRand.close()

def reader(bot, update):
    # Se il verbo può essere diffuso viene inviato il discorso del nostro sommo salvatore richard Stallman
    if "linux" in update.message.text.lower() and not "gnu/linux" in update.message.text.lower():
        interject(bot, update)
        return

    if "gnu/linux" in update.message.text.lower() or "gnu" in update.message.text.lower() and not "ognuno" in update.message.text.lower():
        update.message.reply_text("Stallman approva.")
        log(bot, update, "ha ricevuto la benedizione di Stallman.")
        return

    if "owo" in update.message.text.lower():
        update.message.reply_text("owo")
        log(bot, update, "owo")
        return

    if update.message.chat.type == Chat.PRIVATE:
        # Se l'utente è nella lista di utenti echo, viene ripetuto il messaggio
        with open("log/utentiEcho.txt", "r") as fileEcho:
            utentiEcho = fileEcho.read().splitlines()
            if update.message.chat.username in utentiEcho:
                update.message.reply_text(update.message.text)
                return
        # Altrimenti, viene data una risposta randomica dal file risposte.txt
        with open('fileComandi/risposte.txt') as fileRisposte:
            elencoRisposte = fileRisposte.read().splitlines()
        rand = random.randint(0, len(elencoRisposte)-1)
        update.message.reply_text(elencoRisposte[rand], parse_mode="markdown")
        log(bot, update, "ha usato un comando inesistente : {}".format(update.message.text))
        utentiEcho.close()
    return

# Funzione che scrive a file e a video(console) l'orario e il messaggio
def log(bot, update, string):
    print("{} : {}({}) ".format(datetime.now(), update.message.from_user.id, update.message.from_user.username) + string)
    return

def main():
    global token
    updater = Updater(token)

    updater.dispatcher.add_handler(CommandHandler("start", start))
    updater.dispatcher.add_handler(CommandHandler("help", help))
    updater.dispatcher.add_handler(CommandHandler("info", info))
    updater.dispatcher.add_handler(CommandHandler("interject", interject))
    updater.dispatcher.add_handler(CommandHandler("echo", echo))
    updater.dispatcher.add_handler(CommandHandler("img", img))
    updater.dispatcher.add_handler(CommandHandler("bg", img_pic))
    updater.dispatcher.add_handler(CommandHandler("betterbg", img_file))
    updater.dispatcher.add_handler(CommandHandler("quit", quit))
    updater.dispatcher.add_handler(CommandHandler("roll", die))
    updater.dispatcher.add_handler(CommandHandler("send", send_to_id))
    updater.dispatcher.add_handler(CommandHandler("cid", change_username))

    updater.dispatcher.add_handler(CommandHandler("aiuto", help))
    updater.dispatcher.add_handler(CommandHandler("tira", die))
    updater.dispatcher.add_handler(CommandHandler("esci", quit))
    updater.dispatcher.add_handler(CommandHandler("sfondo", img_pic))
    updater.dispatcher.add_handler(CommandHandler("sfondoBello", img_file))

    updater.dispatcher.add_handler(CommandHandler("invioFile", keyboard))
    updater.dispatcher.add_handler(CallbackQueryHandler(download_method))
    updater.dispatcher.add_handler(CommandHandler("sendFile", keyboard))
    updater.dispatcher.add_handler(CallbackQueryHandler(download_method))
    updater.dispatcher.add_handler(MessageHandler(Filters.text, reader))
    updater.dispatcher.add_handler(MessageHandler(Filters.video, download_video))
    updater.dispatcher.add_handler(MessageHandler(Filters.voice, download_voice))
    updater.dispatcher.add_handler(MessageHandler(Filters.photo, download_photo))
    updater.dispatcher.add_handler(MessageHandler(Filters.document, download_document))
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    os.system("clear")
    print("{} : bot avviato.\n".format(datetime.now()))
    main()
